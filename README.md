# Preudhomme ERP

## Exécution des tests unitaires

Lancez `ng test` pour exécuter les tests unitaires via [Karma](https://karma-runner.github.io).

### Intégration continue (GitLab CI/CD)

Les résultat sont affichés dans l'onglet Tests. Le pourcentage est couverture est affiché sur Job et le rapport de couverture est téléchageable en tant qu'artefact.

#### Paramétrage

Ajoute de la library **karma-junit-reporter** pour pour génération du rapport de test au format xml.
Ajout du type "cobertura" dans "coverageReporter" de karma.config.js et changement de [paramètre Gitlab](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing) pour le pourcentage de couverture.

## Exécution des tests fonctionnels

Exécutez `ng e2e` pour exécuter les tests de bout en bout via [Protractor](http://www.protractortest.org/).

### Intégration continue (GitLab CI/CD)

Le rapport de test e2e est téléchageable en tant qu'artefact.

#### Paramétrage

Ajoute de la library **protractor-beautiful-reporter** pour générer le rapport au format en HTML.

## Serveur de développement

Lancez `ng serve` pour un serveur de développement. Naviguez vers `http://localhost:4200/`. L'application sera automatiquement rechargée si vous changez l'un des fichiers sources.

## Génération du code

Lancez `ng generate component component-name` pour générer un nouveau composant. Vous pouvez également utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Exécutez `ng build` pour construire le projet. Les artefacts de construction seront stockés dans le répertoire `dist/`. Utilisez le drapeau `--prod` pour une construction de production.
