export class HelloWorld {
  /**
   * @example
   * Test method
   * @returns The string "Hello world".
   */
  helloWorld(): string {
    return "Hello world";
  }
}
