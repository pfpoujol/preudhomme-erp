import { HelloWorld } from './hello-world';

describe('HelloWorld', () => {
  it('should create an instance', () => {
    expect(new HelloWorld()).toBeTruthy();
  });
  it(`should return Hello world`, () => {
    const helloWorld = new HelloWorld();
    const str = helloWorld.helloWorld();
    expect(str).toEqual('Hello world');
  });
});
