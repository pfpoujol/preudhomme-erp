// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDnUX4pnS8n_uu6XXsgkBvPgAZCEal6ITc',
    authDomain: 'preudhomme-erp-dev.firebaseapp.com',
    projectId: 'preudhomme-erp-dev',
    storageBucket: 'preudhomme-erp-dev.appspot.com',
    messagingSenderId: '362768195313',
    appId: '1:362768195313:web:cdd738ef107a4743c19350'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
